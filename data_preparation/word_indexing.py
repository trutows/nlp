# Project name: base_models 
# File name: word_indexing
# Created by Tomasz Rutowski at 1/11/19 

"""
    Description: Enables to load original csv file that contains the following columns - processed_path and transcripts.
    The transcript data is used to create two character (char) or word dictionary for the top max_vocs from the entire
    corpus.
"""

import re
import logging
import argparse
import operator

import pandas as pd

from project_helpers import custom_helpers


def main():
    parser = argparse.ArgumentParser("Generate index of words ...")
    parser.add_argument("-l", "--log", default="INFO", help="log level")
    parser.add_argument("-i", "--input_file", help="original csv input file", required=True)
    parser.add_argument("--remove_stopwords", help="path to comma delimited stopwords")
    parser.add_argument("--output_table", required=True, help="seg_key-->mapped transcript (int. seq.)")
    parser.add_argument("--output_dict", required=True, help="output dict")
    parser.add_argument("--mode", choices=['char', 'word'], required=True,
                        help="Select one of the following char or word")
    parser.add_argument("--max_vocs", required=True, type=custom_helpers.check_positive,
                        help="max number of voc for word mode")
    args = parser.parse_args()
    # log_level = args.log

    if args.remove_stopwords is not None:
        remove_stopwords = True
    else:
        logging.warning("remove_stopwords not set")
        remove_stopwords = False

    df = pd.read_csv(args.input_file, header=0, sep='|')
    df = df[['processed_path', 'transcription']]
    df['transcription'] = df['transcription'].apply(lambda row: custom_helpers.remove_special_characters_and_lower(row))
    transcriptions = df.set_index('processed_path')['transcription'].to_dict()

    stopwords = set()
    if remove_stopwords:
        input_lines = [custom_helpers.remove_special_characters_and_lower(line).split(",") for line in
                       open(args.remove_stopwords)]
        for lists in input_lines:
            for elements in lists:
                stopwords.add(elements)

    mapped_transcriptions = {}
    vocs_dict = {}
    vocs_dict["<PAD>"] = 0
    vocs_dict["<START>"] = 1
    vocs_dict["<END>"] = 2

    if args.mode == "char":
        counter = 3
        for key in transcriptions:
            trans = transcriptions[key]
            # trans = str(unicodedata.normalize('NFKD', trans).encode('ascii', 'ignore'))
            trans = trans.strip("\r\n")
            trans = trans.replace("\n", " ")
            trans = trans.replace("\\n", " ")
            trans = trans.replace("\\t", " ")
            trans = trans.replace("\t", " ")
            trans = trans.replace("\s+", " ")
            trans = trans.lower()
            mapped_transcriptions[key] = []
            for c in trans:
                if c not in vocs_dict:
                    vocs_dict[c] = counter
                    counter += 1
                mapped_transcriptions[key].append(vocs_dict[c])
    # TODO: add a mode to use a defined list of words instead of finding most commons.
    elif args.mode == "word":
        all_words_counter = {}
        for key in transcriptions:
            trans = transcriptions[key]
            trans = trans.strip("\r\n")
            trans = trans.replace("\n", " ")
            trans = trans.replace("\\n", " ")
            trans = trans.replace("\\t", " ")
            trans = trans.replace("\t", " ")
            trans = trans.replace("\s+", " ")
            trans = trans.replace("\"", "")
            trans = trans.replace("\'", "")
            trans = trans.replace("\"", "")
            trans = trans.replace("!", "")
            trans = trans.replace("?", "")
            trans = trans.replace(".", "")
            trans = trans.replace(",", "")
            trans = trans.replace("$", "")
            trans = trans.replace(":", "")
            trans = re.sub("\d+th", "", trans)
            trans = re.sub("\d+", "", trans)
            trans = trans.lower()
            words = trans.split()
            filtered = [w for w in words if not w in stopwords]
            if remove_stopwords is True:
                trans = " ".join(filtered)

            transcriptions[key] = trans
            parts = trans.split()
            for p in parts:
                if p not in all_words_counter:
                    all_words_counter[p] = 0
                all_words_counter[p] += 1
        sorted_words = sorted(all_words_counter.items(), key=operator.itemgetter(1), reverse=True)
        # print(dict(sorted_words[:max_vocs]))
        selected_words, _ = list(zip(*sorted_words[:args.max_vocs]))

        counter = 4
        vocs_dict["<UNK>"] = 3
        for key in transcriptions:
            trans = transcriptions[key]
            parts = trans.split()
            mapped_transcriptions[key] = []
            for p in parts:
                if p in selected_words:
                    if p not in vocs_dict:
                        vocs_dict[p] = counter
                        counter += 1
                    mapped_transcriptions[key].append(vocs_dict[p])
                else:
                    mapped_transcriptions[key].append(vocs_dict["<UNK>"])

    else:
        logging.error("mode is not supported!")

    with open(args.output_dict, "w") as fo:
        fo.write("word, id\n")
        for key in vocs_dict:
            fo.write(key + "," + str(vocs_dict[key]) + "\n")

    with open(args.output_table, "w") as fo:
        fo.write("seg_key,transcription\n")
        for key in mapped_transcriptions:
            fo.write(key + "," + " ".join([str(x) for x in mapped_transcriptions[key]]) + "\n")


if __name__ == "__main__":
    main()
