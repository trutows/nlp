# Project name: base_models 
# File name: simple_svm_trimming
# Created by Tomasz Rutowski at 2/7/19 

"""
    Description: This file enables to collect saved data sets that have been trimmed by the number of used tokens and
    calculate optimal hyper parameters in the multi threaded manner

    --input_file - is the location of the folder and primary pickle file which is then striped based on the convention
    developed in the csv_to_trimmed_vectors.py file

    --results is the dictionary that contains hyper parameters for each trimmed threshold

    python simple_svm_trimming_mt.py --input_file '../data_preparation/data/csv_to_vectors_300.pickle'
    --results '../data_preparation/data/best_hypers_trimming.pickle'

"""

from multiprocessing import Pool
from sklearn.svm import SVC
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import os.path

import numpy as np
import pickle
import datetime
import argparse

data = None


def balance_data(input_vector):
    positives, = np.where(input_vector == 1)
    negatives, = np.where(input_vector == 0)
    negatives = np.random.choice(negatives, size=len(positives), replace=False)
    return np.concatenate([positives, negatives])


def get_training():
    training_index = balance_data(data['train']['y_binary'])
    X_train = data['train']['x'][training_index]
    y_train = data['train']['y_binary'][training_index]
    return X_train, y_train


def process_wrapper(params_in):
    iterations = 10
    predictions_svm = None
    for _ in range(iterations):
        x_t, y_t = get_training()
        model = SVC(kernel='rbf', gamma=params_in[0], C=params_in[1], probability=True).fit(x_t, y_t)
        if predictions_svm is None:
            predictions_svm = model.predict_proba(data['dev']['x'])[:, 1]
        else:
            predictions_svm += model.predict_proba(data['dev']['x'])[:, 1]
    predictions_svm = predictions_svm / float(iterations)
    fpr, tpr, t = roc_curve(data['dev']['y_binary'], predictions_svm)
    roc_auc = np.around(auc(fpr, tpr), decimals=4)
    return [params_in, roc_auc, predictions_svm]


def main():
    parser = argparse.ArgumentParser("Generate multiple SVM Hyper parameters")
    parser.add_argument("-i", "--input_file", help="original pickle input file", required=True)
    parser.add_argument("-r", "--results", help="save results to pickle", required=True)
    args = parser.parse_args()
    path_to_load = args.input_file
    hyper_collector_path = args.results
    print(path_to_load)
    print(hyper_collector_path)
    trimming_values = [30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 250]

    if os.path.isfile(hyper_collector_path):
        with open(hyper_collector_path, 'rb') as f:
            hyper_collector = pickle.load(f)
    else:
        hyper_collector = {}

    for t in trimming_values:
        if t in hyper_collector:
            continue

        path_to_load = path_to_load[:-7] + '_' + str(t) + path_to_load[-7:]
        print(path_to_load)
        with open(path_to_load, 'rb') as handle:
            global data
            data = pickle.load(handle)

        gammas = [0.1, 0.5, 1., 2., 5., 7., 10., 15., 20., 25., 30., 35., 40., 50.]
        Cs = [0.001, 0.01, 0.1, 0.3, 0.5, 0.7, 1., 1.5, 2.5, 3.5]
        z = [(a, b) for a in gammas for b in Cs]

        pool = Pool(6)
        results = pool.map(process_wrapper, z)

        hyper_collector[t] = {'roc_auc': results[0][1], 'params': results[0][1], 'predictions': results[0][2]}
        for r in results:
            if hyper_collector[t]['roc_auc'] < r[1]:
                hyper_collector[t]['roc_auc'] = r[1]
                hyper_collector[t]['params'] = r[0]
                hyper_collector[t]['predictions'] = r[2]

        print(datetime.datetime.now(), hyper_collector[t])

        with open(hyper_collector_path, 'wb') as handle:
            pickle.dump(hyper_collector, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()
