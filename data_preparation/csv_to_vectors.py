# Project name: base_models 
# File name: csv_to_vectors
# Created by Tomasz Rutowski at 1/25/19 

"""

    Description:
--input_file
/media/ds/data/projects/eh/data/raw_surveys/amt_data_filtered_00_v6.csv
--output_file
data/csv_to_vectors_300.pickle
--no_less_words
50
--trained_model
/media/ds/data/projects/nlp/trained_models/GoogleNews-vectors-negative300.bin


--input_file
/media/ds/data/projects/eh/data/raw_surveys/amt_data_filtered_00_v6.csv
--output_file
data/csv_to_vectors_elmo.pickle
--no_less_words
50
--trained_model
/media/ds/data/projects/PyCharm/eh_trail/first_model_allen_v3/elmo_2x4096_512_2048cnn_2xhighway_5.5B_weights.hdf5
--model_options
/media/ds/data/projects/PyCharm/eh_trail/first_model_allen_v3/options.json

"""

from collections import Counter
from allennlp.commands.elmo import ElmoEmbedder
import pandas as pd
import gensim as ge
import numpy as np
import warnings
import datetime
import argparse
import pickle
import sys
import gc
import logging

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_colwidth', -1)
pd.set_option('display.width', -1)


def main():
    parser = argparse.ArgumentParser("Convert CSV file to pickle object ...")
    parser.add_argument("-l", "--log", default="INFO", help="log level")
    parser.add_argument("-i", "--input_file", help="original csv input file", required=True)
    parser.add_argument("-o", "--output_file", required=True, help="output pickle file")
    parser.add_argument("-w", "--no_less_words", required=True, help="remove training answers shorter than")
    parser.add_argument("-m", "--trained_model", required=True, help="vectorized model")
    parser.add_argument("-mo", "--model_options", required=False, help="refers to ELMO model")
    # parser.add_argument("-t", "--text_trim", choices=['char', 'word'], required=False,
    #                     help="Select one of the following char or word")
    # parser.add_argument("--max_vocs", required=True, type=custom_helpers.check_positive,
    #                     help="max number of voc for word mode")
    args = parser.parse_args()

    df = pd.read_csv(filepath_or_buffer=args.input_file, header=0, sep='|')

    # df = df.sample(200)

    df.reset_index(inplace=True)
    print('Loaded Info', df.shape)

    selected_columns = ['split', 'transcription', 'AssignmentId', 'question',
                        'bot-topic-1', 'bot-topic-2', 'bot-topic-3', 'other-well-rested', 'personal-ethnicity',
                        'confidence', 'phq-q1', 'phq-q2', 'phq-q3', 'phq-q4', 'phq-q5', 'phq-q6', 'phq-q7',
                        'phq-q8']

    df = df[selected_columns]
    print('Trimmed Columns', df.shape)

    df.drop_duplicates(inplace=True)
    print('After dropping duplicate rows', df.shape)

    df = df[~df.transcription.isna()]
    print('After removing Transcription NaNs', df.shape)

    df = df[~df.transcription.str.contains('AsyncGoogleTranscriber_ERROR')]
    print('After removing Google errors', df.shape)

    df = df[df.question != 'control']
    print('After removing control questions', df.shape)

    df['word_counts'] = df['transcription'].str.split().str.len()

    th_words = int(args.no_less_words)
    df['to_exclude'] = np.where((df.word_counts < th_words) & (df.split == 'train'), 1, 0)
    df = df[df.to_exclude == 0]
    df.drop(['to_exclude'], axis=1, inplace=True)
    print('After removing answers shorter than (only training data set)', th_words, df.shape)

    df.insert(loc=0, column='phq', value=df[selected_columns[-8:]].sum(axis=1))
    df.insert(loc=0, column='phq_binary', value=df.phq.apply(lambda a: 1.0 if a >= 10.0 else 0.0))

    df.drop(selected_columns[-8:], axis=1, inplace=True)
    print('After removing Phq-X columns', df.shape)

    trained_model = None
    collected_exceptions = []

    if args.trained_model[-3:] == 'bin':
        trained_model = ge.models.KeyedVectors.load_word2vec_format(args.trained_model, binary=True)
    elif args.trained_model[-4:] == 'hdf5':
        trained_model = ElmoEmbedder(args.model_options, args.trained_model, cuda_device=0)
    else:
        sys.exit(1)

    def get_vector(input_word):
        try:
            if args.trained_model[-3:] == 'bin':
                return trained_model[input_word]
            elif args.trained_model[-4:] == 'hdf5':
                return trained_model.embed_sentence(input_word)[0][0].astype(np.float)
            else:
                sys.exit(1)
        except:
            collected_exceptions.append(input_word)
            return None

    def custom_text_manipulation(input_text):
        input_text = input_text.lower()
        input_text = input_text.replace("'s", " 's")
        input_text = input_text.replace("'m", " 'm")
        input_text = input_text.replace("'ve", " 've")
        input_text = input_text.replace("'ll", " 'll")
        input_text = input_text.replace("'d", " 'd")
        return input_text.split()

    df.insert(loc=0, column='trans_tokens', value=df['transcription'].apply(lambda a: custom_text_manipulation(a)))
    print(datetime.datetime.now())

    def inference_and_average(row_input):
        local_collector = []
        for token in row_input:
            local_collector.append(get_vector(token))
        local_collector = np.average(np.array(local_collector), axis=0)
        return local_collector

    df.reset_index(inplace=True)
    if args.trained_model[-4:] == 'hdf5':
        collector = []
        for k, values in df.iterrows():
            if k % 25 == 0:
                gc.collect()
                print_output = str(datetime.datetime.now()) + ' progress ' + str(k)
                # print(datetime.datetime.now(), 'progress', k) # , end='\r'
                sys.stdout.write("\r%s" % print_output)
                sys.stdout.flush()
            collector.append(inference_and_average(values.trans_tokens))
        df['trans_vector_average'] = collector
    else:
        df.insert(loc=0, column='trans_vector',
                  value=df['trans_tokens'].apply(lambda a: [get_vector(v) for v in a if get_vector(v) is not None]))

        df.insert(loc=0, column='trans_vec_len', value=df['trans_vector'].apply(lambda a: len(a)))

        df = df[df['trans_vec_len'] > 0]

        df.insert(loc=0, column='trans_vector_average',
                  value=df['trans_vector'].apply(lambda a: np.average(np.array(a), axis=0)))

    print(datetime.datetime.now())

    columns = ['split', 'phq_binary', 'phq', 'trans_vector_average', 'AssignmentId']
    df = df[columns]
    df.reset_index(inplace=True)

    df = df.groupby(['split', 'phq_binary', 'phq', 'AssignmentId'])['trans_vector_average'].apply(list).reset_index(
        name='trans_vector')

    df.insert(loc=0, column='trans_vector_average',
              value=df['trans_vector'].apply(lambda a: np.average(np.array(a), axis=0)))
    df.drop('trans_vector', axis=1, inplace=True)
    print(df.head(10))

    def proper_convert(array_in):
        new_output = []
        for r in array_in:
            new_output.append(list(r))
        return np.array(new_output)

    output = {'train': {}, 'test': {}, 'dev': {}}

    results = df[df.split == 'train']['trans_vector_average'].values
    output['train']['x'] = proper_convert(results)

    results = df[df.split == 'test']['trans_vector_average'].values
    output['test']['x'] = proper_convert(results)

    results = df[df.split == 'dev']['trans_vector_average'].values
    output['dev']['x'] = proper_convert(results)

    output['train']['y_binary'] = np.array(df[df.split == 'train']['phq_binary'].values)
    output['test']['y_binary'] = np.array(df[df.split == 'test']['phq_binary'].values)
    output['dev']['y_binary'] = np.array(df[df.split == 'dev']['phq_binary'].values)

    output['train']['y'] = np.array(df[df.split == 'train']['phq'].values)
    output['test']['y'] = np.array(df[df.split == 'test']['phq'].values)
    output['dev']['y'] = np.array(df[df.split == 'dev']['phq'].values)

    with open(args.output_file, 'wb') as handle:
        pickle.dump(output, handle, protocol=pickle.HIGHEST_PROTOCOL)

    print(output['train']['x'].shape)


if __name__ == "__main__":
    main()
