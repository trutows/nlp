# Project name: base_models 
# File name: custom_helpers
# Created by Tomasz Rutowski at 1/11/19 

"""
    Description:
"""

import logging


def check_positive(value):
    try:
        internal_value = int(value)
        if internal_value <= 0:
            logging.error("check_positive method found negative integer")
            exit(1)
        return internal_value
    except ValueError:
        logging.error("check_positive method did not find an integer")
        exit(1)


def remove_special_characters_and_lower(inputs):
    inputs = inputs.strip()
    inputs = inputs.strip("\r\n")
    inputs = inputs.strip('!,#$%?-')
    inputs = inputs.replace("\n", " ")
    inputs = inputs.replace("\\n", " ")
    inputs = inputs.replace("\\t", " ")
    inputs = inputs.replace("\t", " ")
    inputs = inputs.lower()
    return inputs
